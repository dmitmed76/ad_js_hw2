const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const listContainer = document.createElement('ul');

books.forEach(book => {
    try {
        if (book.author && book.name && book.price) {
            const listItem = document.createElement('li'); 
            listItem.textContent = `${book.name} - ${book.author} - ${book.price}$`; 
            listContainer.appendChild(listItem); 
                } else {
            if(book.author === undefined){
                throw new Error("Author is underfined");
            }   
            if(book.name === undefined){
                throw new Error("Name is underfined");
            } 
            if(book.price === undefined){
                throw new Error("Price is underfined");
            }      
        }

    } catch (error) {

        console.error( error); // Виводимо помилку в консолі, якщо об'єкт книги некоректний
    }
});
console.log(listContainer);
const rootElement = document.getElementById('root'); 
rootElement.appendChild(listContainer); 
